'use strict';
const acorn = require('acorn');
const walk = require('acorn/dist/walk');

function reallyParse (source) {
    try {
        return acorn.parse(source, {
            ecmaVersion: 8,
            ecmaScript: 8,
            allowTrailingComma: true,
            allowReturnOutsideFunction: true,
            allowImportExportEverywhere: true,
            allowHashBang: true
        });
    } catch (ex) {
        return acorn.parse(source, {
            ecmaVersion: 5,
            allowReturnOutsideFunction: true,
            allowImportExportEverywhere: true,
            allowHashBang: true
        });
    }
}

function isLegacyNamespaceDeclaration (node) {
    return node.type === 'CallExpression'
        && node.callee
        && node.callee.type === 'MemberExpression'
        && node.callee.object
        && node.callee.object.name === 'AJS'
        && node.callee.property
        && node.callee.property.name === 'namespace';
}

function isAMDRequireCall (node) {
    return node.type === 'CallExpression'
        && node.callee
        && node.callee.type === 'Identifier'
        && node.callee.name === 'require';
}

function getAMDModuleNameFromRequireCall (node) {
    return node.type === 'Literal' && node.value;
}

function asAMDNode (node) {
    const args = node.arguments;
    if(!args || args.length !==3 || args[1].value!== null) {
        return;
    }
    const globalNamespace = args[0] && args[0].value;
    const possibleAMDRequire = args[2];
    if (!isAMDRequireCall(possibleAMDRequire)) {
        return;
    }
    let moduleName = getAMDModuleNameFromRequireCall(possibleAMDRequire.arguments[0]);

    return  {
        name: globalNamespace,
        moduleName,
        properName: (globalNamespace.split('.').splice(-1)[0]) // TBD maybe fallback to last string in amd module name
    };
}

function detectLegacyFallback (content) {
    const nodes = [];
    const ast = (typeof content === 'string') ? reallyParse(content) : content;

    walk.ancestor(ast, {
        'CallExpression': function (node) {
            if (isLegacyNamespaceDeclaration(node)) {
                const amdNode = asAMDNode(node);
                if (amdNode) {
                    nodes.push(amdNode);
                }
            }
        }
    });

    return nodes;
}

module.exports = detectLegacyFallback;