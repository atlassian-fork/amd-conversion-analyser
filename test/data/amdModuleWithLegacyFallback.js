define("test/data/legacyAmdModule", ["Foobar"], function(Foobar) {
    var foo = true;
    var bar = false;

    return Foobar(foo, bar);
});

AJS.namespace('window.amdModule', null, require("test/data/amdModuleWithLegacyFallback"));